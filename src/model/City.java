package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the city database table.
 * 
 */
@Entity
@NamedQuery(name="City.findAll", query="SELECT c FROM City c")
public class City implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_city")
	private int idCity;

	private String name;

	//bi-directional many-to-one association to Airport
	@OneToMany(mappedBy="city")
	private List<Airport> airports;

	//bi-directional many-to-one association to Trip
	@OneToMany(mappedBy="city1")
	private List<Trip> trips1;

	//bi-directional many-to-one association to Trip
	@OneToMany(mappedBy="city2")
	private List<Trip> trips2;

	public City() {
	}

	public int getIdCity() {
		return this.idCity;
	}

	public void setIdCity(int idCity) {
		this.idCity = idCity;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Airport> getAirports() {
		return this.airports;
	}

	public void setAirports(List<Airport> airports) {
		this.airports = airports;
	}

	public Airport addAirport(Airport airport) {
		getAirports().add(airport);
		airport.setCity(this);

		return airport;
	}

	public Airport removeAirport(Airport airport) {
		getAirports().remove(airport);
		airport.setCity(null);

		return airport;
	}

	public List<Trip> getTrips1() {
		return this.trips1;
	}

	public void setTrips1(List<Trip> trips1) {
		this.trips1 = trips1;
	}

	public Trip addTrips1(Trip trips1) {
		getTrips1().add(trips1);
		trips1.setCity1(this);

		return trips1;
	}

	public Trip removeTrips1(Trip trips1) {
		getTrips1().remove(trips1);
		trips1.setCity1(null);

		return trips1;
	}

	public List<Trip> getTrips2() {
		return this.trips2;
	}

	public void setTrips2(List<Trip> trips2) {
		this.trips2 = trips2;
	}

	public Trip addTrips2(Trip trips2) {
		getTrips2().add(trips2);
		trips2.setCity2(this);

		return trips2;
	}

	public Trip removeTrips2(Trip trips2) {
		getTrips2().remove(trips2);
		trips2.setCity2(null);

		return trips2;
	}
	
	@Override
	public String toString() {
		return "City [idCity = " + idCity + ", name = " + name + "]";
	}

}