package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the trip database table.
 * 
 */
@Entity
@NamedQuery(name="Trip.findAll", query="SELECT t FROM Trip t")
public class Trip implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_trip")
	private int idTrip;

	private float price;

	@Temporal(TemporalType.DATE)
	@Column(name="trip_date")
	private Date tripDate;

	//bi-directional many-to-one association to City
	@ManyToOne
	@JoinColumn(name="arrival_city_id")
	private City city1;

	//bi-directional many-to-one association to City
	@ManyToOne
	@JoinColumn(name="departure_city_id")
	private City city2;

	//bi-directional many-to-one association to Client
	@ManyToOne
	@JoinColumn(name="id_client")
	private Client client;

	//bi-directional many-to-one association to Flight
	@ManyToOne
	@JoinColumn(name="id_flight")
	private Flight flight;

	public Trip() {
	}

	public int getIdTrip() {
		return this.idTrip;
	}

	public void setIdTrip(int idTrip) {
		this.idTrip = idTrip;
	}

	public float getPrice() {
		return this.price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public Date getTripDate() {
		return this.tripDate;
	}

	public void setTripDate(Date tripDate) {
		this.tripDate = tripDate;
	}

	public City getCity1() {
		return this.city1;
	}

	public void setCity1(City city1) {
		this.city1 = city1;
	}

	public City getCity2() {
		return this.city2;
	}

	public void setCity2(City city2) {
		this.city2 = city2;
	}

	public Client getClient() {
		return this.client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Flight getFlight() {
		return this.flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}
	
	@Override
	public String toString() {
		return "Trip [idTrip = " + idTrip + ", price = " + price + ", date = " + tripDate.toString() + ", departureCity = "
				+ city2.getName() + ", arrivalCity = " + city1.getName() + ", flightNo = " + flight.getIdFlight() 
				+ ", clientsName = " + client.getFirstName() + client.getSecondName() + "]";
	}

}