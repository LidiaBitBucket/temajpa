package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the flight database table.
 * 
 */
@Entity
@NamedQuery(name="Flight.findAll", query="SELECT f FROM Flight f")
public class Flight implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_flight")
	private int idFlight;

	@Temporal(TemporalType.DATE)
	@Column(name="flight_date")
	private Date flightDate;

	//bi-directional many-to-one association to Airport
	@ManyToOne
	@JoinColumn(name="arrival_airport_id")
	private Airport airport1;

	//bi-directional many-to-one association to Airport
	@ManyToOne
	@JoinColumn(name="departure_airport_id")
	private Airport airport2;

	//bi-directional many-to-one association to Company
	@ManyToOne
	@JoinColumn(name="flight_id_company")
	private Company company;

	//bi-directional many-to-one association to Plane
	@ManyToOne
	@JoinColumn(name="id_plane")
	private Plane plane;

	//bi-directional many-to-one association to Trip
	@OneToMany(mappedBy="flight")
	private List<Trip> trips;

	public Flight() {
	}

	public int getIdFlight() {
		return this.idFlight;
	}

	public void setIdFlight(int idFlight) {
		this.idFlight = idFlight;
	}

	public Date getFlightDate() {
		return this.flightDate;
	}

	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}

	public Airport getAirport1() {
		return this.airport1;
	}

	public void setAirport1(Airport airport1) {
		this.airport1 = airport1;
	}

	public Airport getAirport2() {
		return this.airport2;
	}

	public void setAirport2(Airport airport2) {
		this.airport2 = airport2;
	}

	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Plane getPlane() {
		return this.plane;
	}

	public void setPlane(Plane plane) {
		this.plane = plane;
	}

	public List<Trip> getTrips() {
		return this.trips;
	}

	public void setTrips(List<Trip> trips) {
		this.trips = trips;
	}

	public Trip addTrip(Trip trip) {
		getTrips().add(trip);
		trip.setFlight(this);

		return trip;
	}

	public Trip removeTrip(Trip trip) {
		getTrips().remove(trip);
		trip.setFlight(null);

		return trip;
	}
	
	@Override
	public String toString() {
		return "Flight [idFlight = " + idFlight + ", arrivalAirport = " + airport1.getName() + ", departureAirport = " + airport2.getName() + ", flightDate = " + flightDate.toString() + ", company ="
				+ company.getName() + ", planeId=" + plane.getIdPlane() + "]";
	}

}