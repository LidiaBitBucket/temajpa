package daolmpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.DatabaseHelper;
import model.City;

public class CitiesDao implements Dao<City> {
	
private DatabaseHelper databaseHelper;
	
	public CitiesDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<City> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(City.class, id));
	}

	@Override
	public List<City> getAll() {
		TypedQuery<City> query = databaseHelper.getEntityManager().createQuery("SELECT s from City s",
				City.class);
		return query.getResultList();
	}

	@Override
	public boolean create(City city) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(city));
	}

	@Override
	public boolean update(City old, City newObj) {
		
		old.setAirports(newObj.getAirports());
		old.setIdCity(newObj.getIdCity());
		old.setTrips1(newObj.getTrips1());
		old.setTrips2(newObj.getTrips2());
		old.setName(newObj.getName());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(City city) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(city));
	}

}
