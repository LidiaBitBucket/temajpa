package daolmpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.DatabaseHelper;
import model.Company;

public class CompaniesDao implements Dao<Company> {
	
private DatabaseHelper databaseHelper;
	
	public CompaniesDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Company> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Company.class, id));
	}

	@Override
	public List<Company> getAll() {
		TypedQuery<Company> query = databaseHelper.getEntityManager().createQuery("SELECT s from Company s",
				Company.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Company company) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(company));
	}

	@Override
	public boolean update(Company old, Company newObj) {
		old.setIdCompany(newObj.getIdCompany());
		old.setName(newObj.getName());
		old.setFlights(newObj.getFlights());
		old.setPlanes(newObj.getPlanes());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Company company) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(company));
	}

}
