package daolmpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.DatabaseHelper;
import model.Client;

public class ClientsDao implements Dao<Client> {
	
private DatabaseHelper databaseHelper;
	
	public ClientsDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Client> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Client.class, id));
	}

	@Override
	public List<Client> getAll() {
		TypedQuery<Client> query = databaseHelper.getEntityManager().createQuery("SELECT s from Client s",
				Client.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Client client) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(client));
	}

	@Override
	public boolean update(Client old, Client newObj) {
		old.setIdClient(newObj.getIdClient());
		old.setBirthDate(newObj.getBirthDate());
		old.setFirstName(newObj.getFirstName());
		old.setSecondName(newObj.getSecondName());
		old.setTelephone(newObj.getTelephone());
		old.setTrips(newObj.getTrips());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Client client) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(client));
	}

}
