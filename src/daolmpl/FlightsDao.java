package daolmpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.DatabaseHelper;
import model.Flight;

public class FlightsDao implements Dao<Flight> {
	
private DatabaseHelper databaseHelper;
	
	public FlightsDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Flight> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Flight.class, id));
	}

	@Override
	public List<Flight> getAll() {
		TypedQuery<Flight> query = databaseHelper.getEntityManager().createQuery("SELECT f from Flight f",
				Flight.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Flight flight) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(flight));
	}

	@Override
	public boolean update(Flight old, Flight newObj) {
		old.setIdFlight(newObj.getIdFlight());
		old.setCompany(newObj.getCompany());
		old.setAirport1(newObj.getAirport1());
		old.setAirport2(newObj.getAirport2());
		old.setFlightDate(newObj.getFlightDate());
		old.setPlane(newObj.getPlane());
		old.setTrips(newObj.getTrips());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Flight flight) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(flight));
	}

}
