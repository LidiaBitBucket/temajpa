package daolmpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.DatabaseHelper;
import model.Trip;

public class TripsDao implements Dao<Trip> {

private DatabaseHelper databaseHelper;
	
	public TripsDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Trip> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Trip.class, id));
	}

	@Override
	public List<Trip> getAll() {
		TypedQuery<Trip> query = databaseHelper.getEntityManager().createQuery("SELECT t from Trip t",
				Trip.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Trip trip) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(trip));
	}

	@Override
	public boolean update(Trip old, Trip newObj) {
		old.setIdTrip(newObj.getIdTrip());
		old.setTripDate(newObj.getTripDate());
		old.setCity1(newObj.getCity1());
		old.setCity2(newObj.getCity2());
		old.setClient(newObj.getClient());
		old.setFlight(newObj.getFlight());
		old.setPrice(newObj.getPrice());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Trip trip) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(trip));
	}
	
}
