package daolmpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.DatabaseHelper;
import model.Plane;

public class PlanesDao implements Dao<Plane> {
	
private DatabaseHelper databaseHelper;
	
	public PlanesDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Plane> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Plane.class, id));
	}

	@Override
	public List<Plane> getAll() {
		TypedQuery<Plane> query = databaseHelper.getEntityManager().createQuery("SELECT p from Plane p",
				Plane.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Plane plane) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(plane));
	}

	@Override
	public boolean update(Plane old, Plane newObj) {
		old.setIdPlane(newObj.getIdPlane());
		old.setCompany(newObj.getCompany());
		old.setCapacity(newObj.getCapacity());
		old.setFlights(newObj.getFlights());
		old.setModel(newObj.getModel());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Plane plane) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(plane));
	}

}
