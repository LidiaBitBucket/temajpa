package daolmpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.DatabaseHelper;
import model.Airport;


public class AirportsDao implements Dao<Airport> {
	
private DatabaseHelper databaseHelper;
	
	public AirportsDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	@Override
	public Optional<Airport> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(Airport.class, id));
	}

	@Override
	public List<Airport> getAll() {
		TypedQuery<Airport> query = databaseHelper.getEntityManager().createQuery("SELECT a FROM Airport a",
				Airport.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Airport airport) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(airport));
	}

	@Override
	public boolean update(Airport old, Airport newObj) {
		
		old.setCity(newObj.getCity());
		old.setFlights1(newObj.getFlights1());
		old.setFlights2(newObj.getFlights2());
		old.setIdAirport(newObj.getIdAirport());
		old.setName(newObj.getName());
		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	@Override
	public boolean delete(Airport airport) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(airport));
	}

}
