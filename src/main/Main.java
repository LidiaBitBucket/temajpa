package main;

import java.util.List;

import daolmpl.AirportsDao;
import helper.DatabaseHelper;
import model.Airport;

public class Main {

	public static void main(String[] args) {

			DatabaseHelper dh = DatabaseHelper.getInstance();
			AirportsDao dao = new AirportsDao(dh);
			List<Airport> airports = dao.getAll();
			
			for (Airport a : airports) {
				System.out.println(a.toString());
			
		}
	}

}
