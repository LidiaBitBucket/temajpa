-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: airport
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `airport`
--

DROP TABLE IF EXISTS `airport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `airport` (
  `id_airport` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `id_city` int(11) NOT NULL,
  PRIMARY KEY (`id_airport`),
  KEY `id_city_idx` (`id_city`),
  CONSTRAINT `id_city` FOREIGN KEY (`id_city`) REFERENCES `city` (`id_city`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `airport`
--

LOCK TABLES `airport` WRITE;
/*!40000 ALTER TABLE `airport` DISABLE KEYS */;
INSERT INTO `airport` VALUES (1,'Charles de Gaulle',1),(2,'Heathrow',2),(3,'Sacramento',3),(4,'Tempelhof',4),(5,'Henri Coanda',5),(6,'Budapest Airport',7),(7,'Le Bourget Airport',1);
/*!40000 ALTER TABLE `airport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `city` (
  `id_city` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_city`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (1,'Paris'),(2,'London'),(3,'San Francisco'),(4,'Berlin'),(5,'Bucuresti'),(6,'Cluj'),(7,'Budapest'),(8,'Madrid'),(9,'Athens'),(10,'Santorini');
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `client` (
  `id_client` int(11) NOT NULL,
  `birth_date` date NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `second_name` varchar(45) NOT NULL,
  `telephone` varchar(10) NOT NULL,
  PRIMARY KEY (`id_client`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (1,'1998-07-01','Schiopu','Anca','0755786567'),(2,'1964-11-25','Marin','Ioana','0755786767'),(3,'1980-07-21','Ionescu','Andrei','0735865778'),(4,'1988-10-15','Jalba','Ana','069697708'),(5,'1989-06-02','Cotelnic','Helen','0016999001');
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `company` (
  `id_company` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id_company`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,'Deutch Airlines'),(2,'Tarom'),(3,'Air Hungary'),(4,'American Express'),(5,'Air France'),(6,'United'),(7,'British Airways'),(8,'KLM'),(9,'United'),(10,'Wizz');
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flight`
--

DROP TABLE IF EXISTS `flight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `flight` (
  `id_flight` int(11) NOT NULL,
  `flight_date` date NOT NULL,
  `departure_airport_id` int(11) NOT NULL,
  `arrival_airport_id` int(11) NOT NULL,
  `id_plane` int(11) NOT NULL,
  `flight_id_company` int(11) NOT NULL,
  PRIMARY KEY (`id_flight`),
  KEY `id_company_idx` (`flight_id_company`),
  KEY `id_plane_idx` (`id_plane`),
  KEY `arrival_airport_id_idx` (`arrival_airport_id`),
  KEY `departure_airport_id_idx` (`departure_airport_id`),
  CONSTRAINT `arrival_airport_id` FOREIGN KEY (`arrival_airport_id`) REFERENCES `airport` (`id_airport`),
  CONSTRAINT `departure_airport_id` FOREIGN KEY (`departure_airport_id`) REFERENCES `airport` (`id_airport`),
  CONSTRAINT `flight_id_company` FOREIGN KEY (`flight_id_company`) REFERENCES `company` (`id_company`),
  CONSTRAINT `id_plane` FOREIGN KEY (`id_plane`) REFERENCES `plane` (`id_plane`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flight`
--

LOCK TABLES `flight` WRITE;
/*!40000 ALTER TABLE `flight` DISABLE KEYS */;
INSERT INTO `flight` VALUES (1,'2019-11-10',1,4,1,1),(2,'2019-12-10',4,5,2,2),(3,'2019-03-15',2,6,3,3),(4,'2019-01-18',5,1,5,5),(5,'2019-07-21',3,6,4,4);
/*!40000 ALTER TABLE `flight` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plane`
--

DROP TABLE IF EXISTS `plane`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `plane` (
  `id_plane` int(11) NOT NULL,
  `model` varchar(45) NOT NULL,
  `capacity` int(11) NOT NULL,
  `id_company` int(11) NOT NULL,
  PRIMARY KEY (`id_plane`),
  KEY `id_company_idx` (`id_company`),
  CONSTRAINT `id_company` FOREIGN KEY (`id_company`) REFERENCES `company` (`id_company`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plane`
--

LOCK TABLES `plane` WRITE;
/*!40000 ALTER TABLE `plane` DISABLE KEYS */;
INSERT INTO `plane` VALUES (1,'Boeing 787-9',150,5),(2,'A380 House Livery',500,4),(3,'Boeing 737-900 ',225,3),(4,'Bombardier Q400 ',310,2),(5,'Embraer E175',90,1),(6,'Embraer E175 ',430,7);
/*!40000 ALTER TABLE `plane` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trip`
--

DROP TABLE IF EXISTS `trip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `trip` (
  `id_trip` int(11) NOT NULL,
  `price` float NOT NULL,
  `trip_date` date NOT NULL,
  `arrival_city_id` int(11) NOT NULL,
  `departure_city_id` int(11) NOT NULL,
  `id_flight` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  PRIMARY KEY (`id_trip`),
  KEY `id_client_idx` (`id_client`),
  KEY `id_flight_idx` (`id_flight`),
  KEY `departure_city_id_idx` (`departure_city_id`),
  KEY `arrival_city_id_idx` (`arrival_city_id`),
  CONSTRAINT `arrival_city_id` FOREIGN KEY (`arrival_city_id`) REFERENCES `city` (`id_city`),
  CONSTRAINT `departure_city_id` FOREIGN KEY (`departure_city_id`) REFERENCES `city` (`id_city`),
  CONSTRAINT `id_client` FOREIGN KEY (`id_client`) REFERENCES `client` (`id_client`),
  CONSTRAINT `id_flight` FOREIGN KEY (`id_flight`) REFERENCES `flight` (`id_flight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trip`
--

LOCK TABLES `trip` WRITE;
/*!40000 ALTER TABLE `trip` DISABLE KEYS */;
INSERT INTO `trip` VALUES (1,360,'2019-11-10',1,4,1,3),(2,150,'2019-12-10',4,5,2,3),(3,360,'2019-03-15',2,6,3,2),(4,560,'2019-03-15',2,6,3,1),(5,90,'2019-01-18',5,1,4,1);
/*!40000 ALTER TABLE `trip` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-11 22:22:22
